import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectionStatusComponent } from './connection-status.component';
import { ConnectionStatusService } from './connection-status.service';

describe('ConnectionStatusComponent', () => {
  let component: ConnectionStatusComponent;
  let fixture: ComponentFixture<ConnectionStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [ConnectionStatusService],
      declarations: [ConnectionStatusComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionStatusComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have no value before oninit', () => {
    expect(component.status).not.toBeDefined();
  });

  it('should return offline', () => {
    const service = <ConnectionStatusService>TestBed.get(ConnectionStatusService);
    spyOn(service, 'getWindow').and.returnValue({ navigator: { onLine: false } });
    fixture.detectChanges();
    expect(service.getWindow).toHaveBeenCalled();
    expect(component.status).toEqual('offline');
  });

  it('should return online', () => {
    const service = <ConnectionStatusService>TestBed.get(ConnectionStatusService);
    spyOn(service, 'getWindow').and.returnValue({ navigator: { onLine: true } });
    fixture.detectChanges();
    expect(service.getWindow).toHaveBeenCalled();
    expect(component.status).toEqual('online');
  });
});
