import { KeyValueList, StringMap } from './interfaces';

export function stringMapToKeyValue(json: StringMap): KeyValueList {
  return Object.keys(json).map(
    key => ({ key, value: json[key] }),
  );
}

export function keyValueToStringMap(keyVal: KeyValueList): StringMap {
  const json = {};

  keyVal.forEach(entry => {
    Object.assign(json, { [entry.key]: entry.value });
  });

  return json;
}
