import { Injectable } from '@angular/core';

import { merge, fromEvent, of, Observable, concat } from 'rxjs';
import { map } from 'rxjs/operators';
import WindowMock from 'window-mock';

@Injectable({
  providedIn: 'root'
})
export class ConnectionStatusService {

  getWindow(): Window {
    return window ? window : new WindowMock() as Window;
  }

  public get connectionStatus(): boolean {
    return this.getWindow().navigator.onLine;
  }

  public get isOnline$(): Observable<boolean> {
    return concat<boolean>(
      of(this.connectionStatus),
      merge<Event>(
        fromEvent(this.getWindow(), 'offline'),
        fromEvent(this.getWindow(), 'online'),
      ).pipe(
        map(x => x.type === 'online'),
      ),
    );
  }
}
