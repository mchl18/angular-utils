import { keyValueToStringMap, stringMapToKeyValue } from './convert';

describe('convert', () => {
  it('should convert json to key value array', () => {

    const input = {
      foo: 'bar',
      baz: 'zulu',
      ter: 'paz',
    };

    const expected = [
      { key: 'foo', value: 'bar' },
      { key: 'baz', value: 'zulu' },
      { key: 'ter', value: 'paz' },
    ];

    expect(stringMapToKeyValue(input)).toEqual(expected)
  });

  it('shoudl convert key value array to json', () => {

    const input = [
      { key: 'foo', value: 'bar' },
      { key: 'baz', value: 'zulu' },
      { key: 'ter', value: 'paz' },
    ];

    const expected = {
      foo: 'bar',
      baz: 'zulu',
      ter: 'paz',
    };

    expect(keyValueToStringMap(input)).toEqual(expected)
  })
});


