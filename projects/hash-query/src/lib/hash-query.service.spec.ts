
import { PLATFORM_ID } from '@angular/core';
import { Location } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';

import { createService } from '@netbasal/spectator';

import { HashQueryService } from './hash-query.service';

describe('HashQueryService', () => {
  const spectator = createService<HashQueryService>({
    service: HashQueryService,
    imports: [RouterTestingModule],
    providers: [
      Location,
      { provide: PLATFORM_ID, useValue: 'browser' },
    ]
  });

  it('should be created', () => {
    expect(spectator.service).toBeTruthy();
  });

  it('should create a key value array from a url with hash query', () => {
    const url = 'https://twitter.com/abc/def#foo=bar;si=so;sa=sa';
    const map = [
      { key: 'foo', value: 'bar' },
      { key: 'si', value: 'so' },
      { key: 'sa', value: 'sa' },
    ];
    expect(spectator.service.stringToKeyValue(url)).toEqual(map);
  });

  it('should create a url from a key value array', () => {
    const map = [
      { key: 'foo', value: 'bar' },
      { key: 'si', value: 'so' },
      { key: 'sa', value: 'sa' },
    ];
    const url = '#foo=bar;si=so;sa=sa;';
    expect(spectator.service.keyValueToString(map)).toEqual(url);
  });
});
