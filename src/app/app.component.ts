import { Component, OnInit, AfterContentInit, AfterViewInit } from '@angular/core';

import { HashQueryService } from 'hash-query';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

  constructor(private hash: HashQueryService) { }

  ngOnInit() {
    this.hash.injectKeyValueList([
      { key: 'size', value: '4' },
      { key: 'price', value: '2' },
      { key: 'zig', value: 'zag' }
    ]);
  }

  ngAfterViewInit() {
    console.log(this.hash.hashQueryMapFromLocation());
  }
}
