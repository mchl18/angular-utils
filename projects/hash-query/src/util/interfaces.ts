export interface KeyValue {
  key: string;
  value: string;
}

export type KeyValueList = KeyValue[];

export interface StringMap {
  [keys: string]: string;
}
