/*
 * Public API Surface of script-secretary
 */

export * from './lib/script-secretary.service';

export * from './lib/uuid';
