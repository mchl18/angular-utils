import { NgModule } from '@angular/core';
import { ConnectionStatusComponent } from './connection-status.component';

@NgModule({
  declarations: [ConnectionStatusComponent],
  exports: [ConnectionStatusComponent]
})
export class ConnectionStatusModule { }
