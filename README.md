# NgUtils

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.0.


## projects

### HashQuery

Service to read and inject query params with self-defined primers and delimiters.

Abusing fragments in the name of SEO.

Usage:

Append query params to current URL
```
this.hashQueryService.injectStringMap([
  { size: 4 },
  { price: 2 },
  { zig: 'zag' }
]);

-> www.host.com/products#size=4;price=2;zig=zag;
```

Read params from current URL, 
```
ngAfterViewInit() {
  this.hashQueryService.hashQueryMapFromLocation();
}
```
