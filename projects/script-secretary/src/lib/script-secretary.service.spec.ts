import { TestBed } from '@angular/core/testing';

import { ScriptSecretaryService } from './script-secretary.service';

import { uuid4 } from './uuid';

describe('ScriptSecretaryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScriptSecretaryService = TestBed.get(ScriptSecretaryService);
    console.log(uuid4());
    expect(service).toBeTruthy();
  });
});
