import { Injectable, Inject, Renderer2, RendererFactory2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { fromEvent, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

interface InjectionScript {
  src: string;
  attributes: { [key: string]: string | number | boolean };
  parent: Element;
  name: string;
  element: HTMLScriptElement;
}

@Injectable({
  providedIn: 'root'
})
export class ScriptSecretaryService {

  private _document: Document;
  private _renderer: Renderer2;
  private _attachedScripts: { [key: string]: InjectionScript } = {};

  constructor(
    @Inject(DOCUMENT) document: Document,
    rendererFactory: RendererFactory2,
  ) {
    this._document = document;
    this._renderer = rendererFactory.createRenderer(null, null);
  }

  public attach(script: InjectionScript): Observable<HTMLScriptElement> {
    const { attributes, parent, src, name } = script;

    if (!name) {
      throw new Error(`
        Script Secretary:

        Need a name for script injection!
      `);
    }

    if (
      !!Object.values(this._attachedScripts).find(s => s.src === src)
    ) {
      throw new Error(`
        Script Secretary:

        Dear Developer, you seem to be loading the same script twice:
        Name: ${name}
        I advice you not to do that.
      `);
    }


    const element: HTMLScriptElement = this._renderer.createElement('script');

    if (attributes) {
      Object.keys(attributes).forEach(key => {
        element.setAttribute(key, `${attributes[key]}`);
      });
    }

    const parentNode = parent ? parent : this._document.body;

    this._renderer.appendChild(parentNode, element);

    return fromEvent<HTMLScriptElement>(element, 'load').pipe(
      map((event: any) => event.srcElement as HTMLScriptElement),
      tap(ref => {
        this._attachedScripts[name] = { ...script, element: ref };
      })
    );
  }

  public remove(name: string) {
    const toBeRemoved = this._attachedScripts[name];

    if (!toBeRemoved) {
      throw new Error(`
        Script Secretary:

        The script you want to remove could not be found: ${name}.
      `);
    }

    const parent = toBeRemoved.parent || this._document.body;

    try {
      this._renderer.removeChild(parent, toBeRemoved.element);
      this._attachedScripts[name] = null;
    } catch (e) {
      throw new Error(`
        Script Secretary:

        Could not remove: ${name}.

        ${e}
      `);
    }
  }
}
