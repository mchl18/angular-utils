import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Location } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ConnectionStatusModule } from 'connection-status';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ConnectionStatusModule,
    RouterModule.forRoot([
      { path: '', component: AppComponent },
    ]),
  ],
  providers: [Location],
  bootstrap: [AppComponent]
})
export class AppModule { }
