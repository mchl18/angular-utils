import * as uuid1 from 'uuid/v1';
import * as uuid3 from 'uuid/v3';
import * as uuid4 from 'uuid/v4';

export { uuid1, uuid3, uuid4 };
