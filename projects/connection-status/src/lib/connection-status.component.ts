import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { ConnectionStatusService } from './connection-status.service';

/**
 * Created in a side project I dropped it in
 * In the top left corner is a hidden element that when hovered shows wether 
 * the browser is offline or online
 */

@Component({
  selector: 'lib-connection-status',
  template: '<ng-content></ng-content>',
})
export class ConnectionStatusComponent implements OnInit {
  public status: string;
  @Output() stateChange = new EventEmitter<string>();

  constructor(
    private connectionStatusService: ConnectionStatusService,
  ) { }

  public ngOnInit() {
    this.connectionStatusService.isOnline$.subscribe(
      res => {
        this.status = res ? 'online' : 'offline';
        this.stateChange.emit(this.status);
      }
    );
  }
}
