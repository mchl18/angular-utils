import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import { Location, isPlatformBrowser } from '@angular/common';

import WindowMock from 'window-mock';

import {
  keyValueToStringMap,
  stringMapToKeyValue,
  KeyValueList,
  StringMap,
} from '../util';

const PARAM_PRIMER = '#';
const KEYVAL_DELIMITER = ';';
const KEYVAL_SEPARATOR = '=';
const LOCATION_STATE = 'Hash Query State';

@Injectable({ providedIn: 'root' })
export class HashQueryService {
  constructor(
    private location: Location,
    @Inject(PLATFORM_ID) private platformId,
  ) { }

  private _paramPrimer = PARAM_PRIMER;
  private _keyValDelimiter = KEYVAL_DELIMITER;
  private _keyValSeparator = KEYVAL_SEPARATOR;
  private _locationState = LOCATION_STATE;

  private get _window(): Window {
    return isPlatformBrowser(this.platformId) ?
      window :
      new WindowMock();
  }

  public setConfig({
    paramPrimer = PARAM_PRIMER,
    keyValueDelimiter = KEYVAL_DELIMITER,
    keyValueSeparattor = KEYVAL_SEPARATOR,
    locationState = LOCATION_STATE,
  }) {
    this._paramPrimer = paramPrimer;
    this._keyValDelimiter = keyValueDelimiter;
    this._keyValSeparator = keyValueSeparattor;
    this._locationState = locationState;
  }

  public stringToKeyValue(url: string): KeyValueList {
    return url
      // split host from query
      // works with www.x.com/abc/#<oarams>
      // or /#<oarams>
      // or #oarams>
      .split(this._paramPrimer)[1]
      // split entries pairs
      .split(this._keyValDelimiter)
      // split key-value pairs
      .map(x => x.split(this._keyValSeparator))
      // map to objects
      .filter(([key, value]) => key && value)
      .map(([key, value]) => ({ key, value }));
  }

  public keyValueToString(map: KeyValueList): string {
    let url = this._paramPrimer;

    map.forEach(({ key, value }) => {
      url = `${url}${key}${this._keyValSeparator}${value}${this._keyValDelimiter}`;
    });

    return url;
  }

  public hashQueryMapFromLocation(): KeyValueList {
    const hashQuery = this.location.path(true);
    return this.stringToKeyValue(hashQuery);
  }

  public stringMapFromLocation(): StringMap {
    return keyValueToStringMap(this.hashQueryMapFromLocation());
  }

  public injectKeyValueList(queryParams: KeyValueList) {
    this._window.history.pushState(
      { queryParams },
      this._locationState,
      `${this.location.path()}/${this.keyValueToString(queryParams)}`
    );
  }

  public injectStringMap(stringMap: StringMap) {
    const queryParams = stringMapToKeyValue(stringMap);
    this._window.history.pushState(
      { queryParams },
      this._locationState,
      `${this.location.path()}/${this.keyValueToString(queryParams)}`
    );
  }
}

