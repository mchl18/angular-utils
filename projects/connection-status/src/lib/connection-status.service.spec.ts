import { TestBed } from '@angular/core/testing';

import { ConnectionStatusService } from './connection-status.service';

describe('ConnectionStatusService', () => {
  let service: ConnectionStatusService;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(ConnectionStatusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return a window object', () => {
    expect(service.getWindow()).toBeTruthy();
  });

  it('should return false', () => {
    spyOn(service, 'getWindow').and.returnValue({ navigator: { onLine: false } });
    expect(service.connectionStatus).toEqual(false);
  });

  it('should return true', () => {
    spyOn(service, 'getWindow').and.returnValue({ navigator: { onLine: true } });
    expect(service.connectionStatus).toEqual(true);
  });
});
